<?php
namespace Garradin;
use Garradin\Entities\Files\File;

$db = DB::getInstance();

$db->import(dirname(__FILE__) . "/data/schema.sql");

$path = __DIR__.'/data/default_sign.png';
$png = (new File)->createAndStore('skel/plugin/facturation','sign.png', $path, null);