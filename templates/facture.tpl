{include file="admin/_head.tpl" title="Document — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{form_errors}

{if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE)}
{linkbutton shape="edit" href="%sfacture_modifier.php?id=%d"|args:$plugin_url,$facture.id label="Modifier ce document"}
{linkbutton shape="plus" href="%sfacture_ajouter.php?copy=%d"|args:$plugin_url,$facture.id label="Dupliquer ce document"}
{/if}

{linkbutton shape="download" href="%spdf.php?d&id=%d"|args:$plugin_url,$facture.id label="Télécharger ce document"}

{if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN)}
{linkbutton shape="delete" href="%sfacture_supprimer.php?id=%d"|args:$plugin_url,$facture.id label="Supprimer ce document"}
{/if}

<div style="margin-top: 1em; width: min-content;">
    <embed src="pdf.php?id={$id}" width="840px" height="1188px" style="max-width: 900px; border: 1px solid black;">
</div>
{include file="admin/_foot.tpl"}
