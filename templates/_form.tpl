{form_errors}

<form method="post" action="{$self_url}">

	<fieldset>
		<legend>Type d'écriture</legend>
		<dl>
		{foreach from=$types_details item="type"}
			<dd class="radio-btn">
				{input type="radio" name="type" value=$type.id source=$radio label=null}
				<label for="f_type_{$type.id}">
					<div>
						<h3>{$type.label}</h3>
						{if !empty($type.help)}
							<p>{$type.help}</p>
						{/if}
					</div>
				</label>
			</dd>
		{/foreach}
		</dl>
	</fieldset>

	<fieldset>
		<legend data-types="t0">Créer un devis</legend>
		<legend data-types="t1">Créer une facture</legend>
		<legend data-types="t2">Créer un reçu fiscal</legend>
		<legend data-types="t3">Créer un reçu de cotisation</legend>
		<dl>

			{input type="text" name="numero_facture" maxlength=12 label="Numéro du document" required=$require_number source=$doc}

			<dd class="help">
				{if $require_number}
					Chaque document doit comporter un numéro unique délivré chronologiquement et de façon continue. Il faut que le système adopté par l'association garantisse que deux factures émises la même année ne peuvent pas porter le même numéro.
				{else}
					Laisser vide pour qu'un numéro soit automatiquement affecté au format <code>{$number_pattern}</code>.
				{/if}

			{input type="date" name="date_emission" default=$date label="Date d'émission" required=1 source=$doc}
			<dd class="help" data-types="t2">
				<p>Date du versemen du don</p>
			</dd>
			<div data-types="t0 t1 t2">
			{input type="date" name="date_echeance" default=$date label="Date d'échéance" required=1 source=$doc}
				<dd class="help" data-types="t2">
					<p>Date d'établissement du document</p>
				</dd>
			</div>

			<dt><label>Statut</label></dt>
			
			{input type="checkbox" name="reglee" value="1" label="Réglée" source=$doc data-types="t1"}
			<div data-types="t0 t1 t2">
			{input type="checkbox" name="archivee" value="1" label="Archivée" source=$doc disabled="disabled"}
			</div>

		</dl>
	</fieldset>

	<fieldset data-types="t0 t1 t2">
		<legend>Client</legend>

		<dl>
			<dt><label>Document adressé à :</label></dt>
			{if !empty($clients)}
			<dd>
				{input type="radio" name="base_receveur" value="membre" label="Un·e membre" default=1 source=$doc}
				{input type="radio" name="base_receveur" value="client" label="Un·e client·e" source=$doc}
			</dd>
			{/if}
		</dl>

		<dl class="type_membre">
			{input type="select" name="membre" label="Membre" options=$membres required=1 source=$doc}
		</dl>

		{if !empty($clients)}
			<dl class="type_client">
				{input type="select" name="client" label="Client" options=$clients required=1 class="type_client" source=$doc}
			</dl>
		{else}
			<input type="hidden" name="base_receveur" value="membre" />
		{/if}
	</fieldset>

	<fieldset data-types="t0 t1">
		<legend>Contenu</legend>

		<dl>
			{input type="select" name="moyen_paiement" required=1 label="Moyen de paiement" source=$doc options=$moyens_paiement default=$doc.moyen_paiement}

			<dt><label for="f_contenu">Contenu du document</label><dt>
			<dd>
				<table class="list" style="max-width: 800px;">
					<colgroup>
						<col width="65%">
						<col width="33%">
						<col width="2%">
					</colgroup>
					<thead>
						<tr>
							<td>Désignation</td>
							<td>Prix</td>
							<td></td>
						</tr>
					</thead>
					<tbody id="Lines">
					{if count($designations) > 0}
						<tr id="Line1" class="hidden">
							<td><textarea name="designation_tpl[]" style="width:98%;"></textarea></td>
							{money_fac name="prix_tpl[]"}
							<td class="fact_rm_line">{button label="Enlever" title="Enlever la ligne" shape="minus" min="2" name="remove_line"}</td>
						</tr>

						{foreach from=$designations item=designation key=key}							
						<tr>
							<td><textarea name="designation[]" style="width:98%;">{$designation}</textarea></td>
							{money_fac value=$prix[$key] user=$from_user}
							<td class="fact_rm_line">{button label="Enlever" title="Enlever la ligne" shape="minus" min="2" name="remove_line"}</td>
						</tr>
						{/foreach}
					{else}			
						<tr id="Line1" class="hidden">
							<td><textarea name="designation_tpl[]" style="width:98%;"></textarea></td>
							{money_fac name="prix_tpl[]"}
							<td class="fact_rm_line">{button label="Enlever" title="Enlever la ligne" shape="minus" min="2" name="remove_line"}</td>
						</tr>
					{/if}
					</tbody>
					<tfoot>
						<tr>
							<td style="text-align: right;">Total :</td>
							<td><span id="total">0.00</span> €</td>
							<td>{button label="Ajouter" title="Ajouter une ligne" id="ajouter_ligne" shape="plus"}</td>
						</tr>
					</tfoot>
				</table>
			</dd>
		</dl>
	</fieldset>
	
	<fieldset data-types="t2">
		<legend>Contenu</legend>
		<dl>
			{input type="money" name="total" label="Montant du don" required=1 source=$doc default="0,0"}
			{input type="select" name="forme_don" required=1 label="Forme du don" source=$doc options=$formes_don default=$doc.forme_don}
			{input type="select" name="nature_don" required=1 label="Nature du don" source=$doc options=$natures_don default=$doc.nature_don}
			{input type="select" name="texte_don" required=1 label="Texte explicatif" source=$doc options=$textes_don default=$doc.texte_don}
			{input type="select" name="moyen_paiement2" required=1 label="Moyen de paiement" source=$doc options=$moyens_paiement default=$doc.moyen_paiement}
		</dl>

	</fieldset>

	<p class="submit" data-types="t0 t1 t2">
		{csrf_field key=$csrf_key}
        {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
	</p>

	<fieldset data-types="t3">
		<legend>Membre</legend>
		<dl>
			<dt><label>Reçu adressée à :</label></dt>
			<dd>
				{input type="select" name="membre_cotis" label="Membre" options=$membres required=1 default=$doc.membre}
			</dd>
		</dl>
	</fieldset>

	<p class="submit" data-types="t3">
		{csrf_field key="add_cotis_1"}
        {button type="submit" name="select_cotis" label="Sélectionner" shape="right" class="main"}
	</p>


{if $step}
	<fieldset data-types="t3">
		<legend>Cotisation</legend>
	{if count($liste)}
		<dl>
			<dt>Sélectionnez la cotisation concernée :</dt>

			<table class='list'>
				<thead>
					<td></td>
					<td>Id</td>
					<td>Intitulé</td>
					<td>Date d'inscription</td>
					<td>Expiration d'expiration</td>
					<td>Tarif</td>
					<td>Montant</td>
					<td>Somme payée</td>
				</thead>

				{foreach from=$liste item=cotis key=i}
					{if !$cotis.paid}
						{continue}
					{/if}
					<tr>
						<td>
							{input type="radio" name="cotisation" value="%s"|args:$i}
						</td>
						{foreach from=$cotis item=element key=key}
							{if $key == 'paid'}
								{continue}
							{/if}
							<td>
									<label for="f_cotisation_{$i}">
									{if ($key == 'date' || $key == 'expiry') && $element > 0}
										{$element|date_short}
									{elseif $key == 'amount' OR $key == 'paid_amount'}
										{$element|raw|money_currency}
									{else}
										{$element}
									{/if}

										<input type="hidden" name="{$key}_{$i}" value="{$element}">
									</label>
							</td>
						{/foreach}
					</tr>
				{/foreach}
			</table>

		</dl>
	</fieldset>

	<p class="submit" data-types="t3">
		{csrf_field key="add_cotis_2"}
        {button type="submit" name="add_cotis" label="Enregistrer" shape="right" class="main"}
	</p>
	{else}
		<p>Ce membre n'a aucune cotisation payée.</p>
	</fieldset>
	{/if}
{/if}

</form>

{include file="%s/templates/_js.tpl"|args:$plugin_root}
