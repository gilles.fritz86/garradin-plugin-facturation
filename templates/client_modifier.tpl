{include file="admin/_head.tpl" title="Modifier un client — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=0}
{include file="%s/templates/_menu_client.tpl"|args:$plugin_root current="client_modifier"}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Modifier un client</legend>
        <dl>
            {input type="text" name="nom" label="Nom" required=true source=$client}
            {input type="text" name="adresse" label="Adresse" required=true source=$client}
            {input type="text" name="code_postal" label="Code postal" required=true source=$client}
            {input type="text" name="ville" label="Ville" required=true source=$client}
            {input type="tel" name="telephone" label="Téléphone" source=$client}
            {input type="email" name="email" label="Adresse e-mail" source=$client}
        </dl>
    </fieldset>

    <p class="submit">
        {csrf_field key="edit_client"}
        {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
</form>


{include file="admin/_foot.tpl"}