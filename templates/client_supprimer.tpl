{include file="admin/_head.tpl" title="Supprimer un client — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=0}
{include file="%s/templates/_menu_client.tpl"|args:$plugin_root current="client_supprimer"}

{if !$deletable}
    <p class="error block">Ce/cette client·e ne peut pas être supprimé·e car des documents lui y sont liés.</p>
{else}

    {include file="common/delete_form.tpl"
        legend="Supprimer ce client ?"
        warning="Êtes-vous sûr de vouloir supprimer le client « %s » ?"|args:$client.nom
        alert="Attention, cette action est irréversible."}

{/if}

{include file="admin/_foot.tpl"}