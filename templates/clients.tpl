{include file="admin/_head.tpl" title="Clients — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="clients"}

{if $list->count()}
    {include file="common/dynamic_list_head.tpl"}

        {foreach from=$list->iterate() item="row"}
            <tr>
                <td>{$row.id}</td>
                <th><a href="client.php?id={$row.id}">{$row.nom}</a></th>

                {foreach from=$row item="value" key="key"}
                    {if $key == 'id' || $key == 'nom'}
                        <?php continue; ?>
                    {/if}
                    <td>{$value}</td>
                {/foreach}
                <td class="actions">
                    {if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN)}
                        {linkbutton shape="delete" href="client_supprimer.php?id=%d"|args:$row.id label="Supprimer"}
                    {/if}
                    {if $session->canAccess($session::SECTION_USERS, $session::ACCESS_ADMIN)}
                        {linkbutton shape="edit" href="client_modifier.php?id=%d"|args:$row.id label="Modifier"}
                    {/if}
                    {linkbutton shape="user" href="client.php?id=%d"|args:$row.id label="Fiche client"}
                </td>
            </tr>
        {/foreach}

        </tbody>
    </table>

    <p class="help">
        Export de la liste&nbsp;:
        {linkbutton href="?export=csv" label="Export CSV" shape="download"}
        {linkbutton href="?export=ods" label="Export tableur" shape="download"}
    </p>

{else}
    <p class="alert block">
        Aucun client trouvé.
    </p>
{/if}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Ajouter un client</legend>
        <dl>
            {input type="text" name="nom" label="Nom" required=true}
            {input type="text" name="adresse" label="Adresse" required=true}
            {input type="text" name="code_postal" label="Code postal" required=true}
            {input type="text" name="ville" label="Ville" required=true}
            {input type="tel" name="telephone" label="Téléphone"}
            {input type="email" name="email" label="Adresse e-mail"}
        </dl>
    </fieldset>

    <p class="submit">
        {csrf_field key="add_client"}
        {button type="submit" name="add" label="Enregistrer" shape="right" class="main"}
    </p>
</form>


{include file="admin/_foot.tpl"}
