{include file="admin/_head.tpl" title="Configuration — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="config"}

{if $ok && !$form->hasErrors()}
    <p class="block confirm">
        La configuration a bien été enregistrée.
    </p>
{/if}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Informations de l'association</legend>
        <dl>
            {input type="text" name="rna_asso" label="RNA de l'association" source=$plugin.config}
            {input type="text" name="siret_asso" label="SIRET de l'association" source=$plugin.config}
            {input type="checkbox" name="ttc" value="1" label="L'association émet des factures TTC (TVA)" source=$plugin.config}
        </dl>
        <br>
        <fieldset>
            <legend>Adresse</legend>
            <dl>
                {input type="text" name="numero_rue_asso" source=$plugin.config label="Numéro de rue" maxlength=5}
                {input type="text" name="rue_asso" source=$plugin.config label="Nom de rue"}
                {input type="text" name="cp_asso" source=$plugin.config label="Code postal"}
                {input type="text" name="ville_asso" source=$plugin.config label="Ville"}
            </dl>
        </fieldset>
            <fieldset>
                <legend>Objet</legend>
                <dl>
                    <dt><label>L'objet (but) de l'association doit tenir sur 3 lignes, chaque ligne pouvant accueillir un maximum de 100 caractères.</label><b title="(Champ obligatoire)">obligatoire pour reçus fiscaux</b></dt>
                    {input type="text" name="objet_0" source=$plugin.config label="Ligne 1" maxlength=95}
                    {input type="text" name="objet_1" source=$plugin.config label="Ligne 2" maxlength=95}
                    {input type="text" name="objet_2" source=$plugin.config label="Ligne 3" maxlength=95}
                </dl>
            </fieldset>

            <fieldset>
                <legend>Droit à la réduction d'impôt</legend>
                <dl>
                    <dt><label>Articles concernés par l'association :</label> <b title="(Champ obligatoire)">obligatoire pour reçus fiscaux</b></dt>
                    {input type="checkbox" name="droit_art200" value="1" source=$plugin.config label="Article 200"}
                    {input type="checkbox" name="droit_art238bis" value="1" source=$plugin.config label="Article 238 bis"}
                    {input type="checkbox" name="droit_art885-0VbisA" value="1" source=$plugin.config label="Article 885-0V bis A"}
                </dl>
            </fieldset>

    </fieldset>

    <fieldset>
        <legend>Factures</legend>
        <dl>
            {input type="textarea" class="full-width" rows="10" name="footer" source=$plugin.config label="Pied de document — informations légales" required=true}
        </dl>
    </fieldset>

    <fieldset>
        <legend>Configuration du plugin</legend>
        <dl>
            {input type="checkbox" name="validate_cp" value="1" source=$plugin.config label="Vérifier le code postal lors de saisie/modification de client (seulement FR)"}
            {input type="checkbox" name="unique_client_name" value="1" source=$plugin.config label="Noms des clients uniques"}
            {input type="select" name="pattern" label="Format de numéro de document" required=false options=$patterns source=$plugin.config}
            <dd class="help">
                F = Facture, D = Devis, RF = Reçu fiscal, RC = Reçu cotisation
            </dd>
        </dl>
        <i>Pour personnaliser l'apparence de la facture, il faut pour l'instant se retrousser les manches et éditer soi-même le fichier www/admin/pdf.php du plugin ! </i>
    </fieldset>

    <p class="submit">
        {csrf_field key="facturation_config"}
        {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
</form>


<form method="post" enctype="multipart/form-data" action="{$self_url|escape}" id="f_upload">
    <fieldset>
        <legend>Signature du responsable</legend>
        <p>
            Uploadez votre signature dans les documents, onglet Squelettes du site web, sous plugin/facturation/sign.png
            <br>
            Il est préférable d'avoir un fond transparent.
        </p>
        <br>

        <p>
            La signature actuelle :
            <br>
            <img src="{$www_url}plugin/facturation/sign.png">
        </p>

        <p>A cause de l'implémentation de la signature, votre signature est acessible publiquement à l'adresse suivante : {$www_url}plugin/facturation/sign.png !</p>
    </fieldset>
</form>

{include file="admin/_foot.tpl"}