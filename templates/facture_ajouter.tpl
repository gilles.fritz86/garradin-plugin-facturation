{include file="admin/_head.tpl" title="Créer un document — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="facture"}

{include file="%s/templates/_form.tpl"|args:$plugin_root}

{include file="%s/templates/_js.tpl"|args:$plugin_root}

{include file="admin/_foot.tpl"}
