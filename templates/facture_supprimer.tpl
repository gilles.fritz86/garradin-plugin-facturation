{include file="admin/_head.tpl" title="Supprimer un document — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=0}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{form_errors}

<form method="post" action="{$self_url}">

    <fieldset>
        <legend>Supprimer ce document ?</legend>
        <h3 class="warning">
            Êtes-vous sûr de vouloir supprimer le document numéro «&nbsp;{$doc.numero}&nbsp;» ?
        </h3>
        <p class="alert">
            <strong>Attention</strong> : cette action est irréversible.
        </p>
    </fieldset>

    <p class="submit">
        {csrf_field key="delete_doc_"|cat:$doc.id}
        <input type="submit" name="delete" value="Supprimer &rarr;" />
    </p>

</form>

{include file="admin/_foot.tpl"}