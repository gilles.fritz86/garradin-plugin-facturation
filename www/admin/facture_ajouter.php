<?php

namespace Garradin;

use const \Garradin\Plugin\Facturation\PATTERNS_LIST;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

use Garradin\DB;
use stdClass;

$db = DB::getInstance();

$step = $radio = false;
$liste = [];
$designations = [];
$prix = [];

$csrf_key = 'ajout_facture';
$fields = $facture->recu_fields;

$moyens_paiement = $facture->listMoyensPaiement(true);

$doc = null;
$require_number = $plugin->getConfig('pattern') ? false : true;

if (qg('copy') !== null && $f = $facture->get((int)qg('copy'))) {
    $doc = (array) $f;

    // Copié depuis facture_modifier.php
    $doc['type']           = $f->type_facture;
    $doc['numero_facture'] = '';
    $doc['base_receveur']  = $f->receveur_membre ? 'membre' : 'client';
    $doc['client']         = $f->receveur_id;
    $doc['membre']         = $f->receveur_id;

    if ( $f->type_facture == CERFA ) {
        $doc['forme_don'] = $f->contenu['forme'];
        $doc['nature_don'] = $f->contenu['nature'];
        $doc['texte_don'] = $f->contenu['texte'];
    }
}

$tpl->assign('require_number', $require_number);
$tpl->assign('number_pattern', PATTERNS_LIST[$plugin->getConfig('pattern')]);

$tpl->assign('moyens_paiement', $moyens_paiement);
$tpl->assign('moyen_paiement', f('moyen_paiement') ?: 'ES');

$tpl->assign('formes_don', array('1' => 'Acte authentique',
                                 '2' => 'Acte sous seing privé',
                                 '3' => 'Don manuel',
                                 '4' => 'Autres'));
$tpl->assign('natures_don', array('1' => 'Numéraire',
                                 '2' => 'Chèque',
                                 '3' => 'Virement, CB; ...'));
$tpl->assign('textes_don', $facture->listTextesCerfa());

if (f('save'))
{
    $form->check($csrf_key, [
        'type' => 'required|in:'.implode(',', [DEVIS, FACT, CERFA]),
        'numero_facture' => $require_number ? 'required|string' : 'string',
        'date_emission' => 'required|date_format:d/m/Y',
        'date_echeance' => 'required|date_format:d/m/Y',
        // 'reglee' => '',
        // 'archivee' => '',
        'base_receveur' => 'required|in:membre,client',
        // 'client' => '',
        // 'membre' => '',
        'moyen_paiement' => 'required|in:' . implode(',', array_keys($moyens_paiement)),
        'designation' => 'array|required',
        'prix' => 'array|required'
    ]);

    if (!$form->hasErrors())
    {
        try
        {
            if ( count(f('designation')) !== count(f('prix')) )
            {
                throw new UserException('Nombre de désignations et de prix reçus différent.');
            }

            $truc = [
                'numero' => f('numero_facture'),
                'date_emission' => f('date_emission'),
                'date_echeance' => f('date_echeance'),
                'reglee' => f('reglee') == 1?1:0,
                'archivee' => f('archivee') == 1?1:0,
                'moyen_paiement' => f('moyen_paiement'),
                'toto' => 0
            ];
            $truc['type_facture'] = f('type');
            if (in_array(f('type'), [DEVIS, FACT]))
            {
            

                foreach(f('designation') as $k=>$value)
                {
                    $truc['contenu'][$k]['designation'] = $value;
                    $truc['contenu'][$k]['prix'] = Utils::moneyToInteger(f('prix')[$k]);
                    $truc['toto'] += Utils::moneyToInteger(f('prix')[$k]);
                }
                $truc['total'] = $truc['toto'];
                unset($truc['toto']);
            }
            elseif (f('type') == CERFA) 
            {
                $truc['moyen_paiement'] = f('moyen_paiement2');
                $truc['contenu'] = [
                    'forme' => f('forme_don'),
                    'nature' => f('nature_don'),
                    'texte' => f('texte_don')];
                unset($truc['toto']);
                $truc['total'] = Utils::moneyToInteger(f('total'));
            }
            if (f('base_receveur') == 'client')
            {
                $truc['receveur_membre'] = 0;
                $truc['receveur_id'] = f('client');
            }
            elseif (f('base_receveur') == 'membre')
            {
                $truc['receveur_membre'] = 1;
                $truc['receveur_id'] = f('membre');
            }

            $id = $facture->add($truc, $plugin->getConfig('pattern'));

            Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);

        }
        catch(UserException $e)
        {
            $form->addError($e->getMessage());
        }
    }

}
elseif (f('select_cotis'))
{
    $form->check('add_cotis_1',[
        'numero_facture' => 'required|string',
        'date_emission' => 'required|date_format:d/m/Y',
        'membre_cotis' => 'required|numeric',
    ]);

    $step = true;
}
elseif (f('add_cotis'))
{
    $form->check('add_cotis_2',[
        'numero_facture' => 'required|string',
        'date_emission' => 'required|date_format:d/m/Y',
        'membre_cotis' => 'required|numeric',
        'cotisation' => 'required',
        ]);

    $radio['type'] = f('cotisation');
    
    if (!$form->hasErrors())
    {
        try
        {
            $num = (int) str_replace('cotis_', '', $radio['type']);
            foreach($fields as $field)
            {
                $cotis[$field] = f($field.'_'.$num);
            }
    
            $r = $facture->getCotis(f('membre_cotis'), $cotis['id']);
            $r = $r[0];

            $data = [
                'type_facture' => COTIS,
                'numero' => f('numero_facture'),
                'receveur_membre' => 1,
                'receveur_id' => f('membre_cotis'),
                'date_emission' => f('date_emission'),
                'moyen_paiement' => f('moyen_paiement'),
                'total' => $r->paid_amount ?? $r->amount,
                'contenu' => ['id' => $cotis['id'],
                            'intitule' => $cotis['label'],
                            'souscription' => $cotis['date'],
                            'expiration' => $cotis['expiry'] ]
            ];

            $id = $facture->add($data, $plugin->getConfig('pattern'));

            Utils::redirect(PLUGIN_URL . 'facture.php?id='.(int)$id);
        }
        catch (UserException $e)
        {
            $form->addError($e->getMessage());
        }
    }

    $step = true;
}

if ($step)
{
    try
    {
        $liste = $facture->getCotis((int)f('membre_cotis'));
    }
    catch (UserException $e)
    {
        $form->addError($e->getMessage());
    }
}


$type = qg('t') ? (int) qg('t') : null;

if (in_array($type, [DEVIS, FACT, CERFA, COTIS], true))
{
    $radio['type'] = $type;
}
elseif (null !== f('type'))
{
    $radio['type'] = f('type');
}
elseif (isset($doc['type'])) {
    $radio['type'] = $doc['type'];
}
else 
{
    $radio['type'] = FACT;
}


$tpl->assign('types_details', $facture->types);

$tpl->assign('client_id', f('client') ?: -1);
$tpl->assign('membre_id', f('membre') ?: -1);

$from_user = false;
if (($d = f('designation')) && ($p = f('prix')) && implode($d))
{
    foreach($d as $k=>$v)
    {
        if (empty($v) && empty($p[$k]))
        {
            continue;
        }
        $designations[] = $v;
        $prix[] = $p[$k];
    }
    $from_user = true;
}
else if (!empty($doc['contenu'])) {
    foreach($doc['contenu'] as $k=>$v)
    {
        if (empty($v['designation']) && empty($v['prix']))
        {
            continue;
        }
        $designations[] = $v['designation'];
        $prix[] = $v['prix'];
    }
}
else {
    $designations = ['Exemple'];
    $prix = [250];
}

$tpl->assign(compact('liste', 'radio', 'step'));

$date = new \DateTime;
$date->setTimestamp(time());
$tpl->assign('date', $date->format('d/m/Y'));

$tpl->assign(compact('designations', 'prix', 'from_user', 'identite', 'csrf_key', 'doc'));
$tpl->assign('membres', $db->getAssoc('SELECT id, '.$identite.' FROM membres WHERE id_category != -2 NOT IN (SELECT id FROM users_categories WHERE hidden = 1);'));
$tpl->assign('clients', $db->getAssoc('SELECT id, nom FROM plugin_facturation_clients;'));

$tpl->display(PLUGIN_ROOT . '/templates/facture_ajouter.tpl');
