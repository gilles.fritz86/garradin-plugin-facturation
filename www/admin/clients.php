<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

if(f('add'))
{
	$form->check('add_client', [
		'nom' => 'required|string',
		'adresse' => 'required|string',
		'code_postal' => 'required|string',
		'ville' => 'required|string',
		'telephone' => 'string',
		'email' => 'email'
	]);

	if (!$form->hasErrors())
	{
		try
		{
			$id = $client->add([
				'nom'			=>  f('nom'),
				'adresse'		=>  f('adresse'),
				'code_postal'	=>  f('code_postal'),
				'ville'			=>  f('ville'),
				'telephone'		=>  f('telephone'),
				'email'			=>  f('email')
			]);

			$id ? Utils::redirect(PLUGIN_URL . 'client.php?id='.(int)$id):'';
		}
		catch (UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}
}


$list = $client->list();
$list->loadFromQueryString();

$tpl->assign(compact('list'));

$tpl->display(PLUGIN_ROOT . '/templates/clients.tpl');
