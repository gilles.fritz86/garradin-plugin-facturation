<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

qv(['id' => 'required|numeric']);

$id = (int) qg('id');

$c = $client->get($id);

if (!$client)
{
	throw new UserException("Ce client n'existe pas.");
}

$csrf_key = 'delete_client_'.$c->id;

$form->runIf('delete', function () use ($client, $c) {
	$client->delete($c->id);
}, $csrf_key, PLUGIN_URL . 'clients.php');

$tpl->assign('deletable', $client->isDeletable($id));
$tpl->assign('client', $c);
$tpl->assign(compact('csrf_key'));
$tpl->display(PLUGIN_ROOT . '/templates/client_supprimer.tpl');
