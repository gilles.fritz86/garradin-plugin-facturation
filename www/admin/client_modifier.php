<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

qv(['id' => 'required|numeric']);
$id = (int) qg('id');

$c = $client->get($id);

if (!$c)
{
	throw new UserException("Ce client n'existe pas.");
}

if(f('save'))
{
	$form->check('edit_client', [
		'nom'			=> 'required|string',
		'adresse'		=> 'required|string',
		'code_postal'	=> 'required|string',
		'ville'			=> 'required|string',
		'telephone'		=> 'string',
		'email'			=> 'email'
	]);

	if (!$form->hasErrors())
	{
		try
		{
			$r = $client->edit($id,[
				'nom'			=>  f('nom'),
				'adresse'		=>  f('adresse'),
				'code_postal'	=>  f('code_postal'),
				'ville'			=>  f('ville'),
				'telephone'		=>  f('telephone'),
				'email'			=>  f('email')
			]);

			$r ? Utils::redirect(PLUGIN_URL . 'client.php?id='.(int)$id):'';
		}
		catch (UserException $e)
		{
			$form->addError($e->getMessage());
		}
	}
}


$tpl->assign('client', $c);
$tpl->display(PLUGIN_ROOT . '/templates/client_modifier.tpl');
