<?php

namespace Garradin\Plugin\Facturation;

use Garradin\Config;
use Garradin\Utils;

define('DEVIS', 0);
define('FACT', 1);
define('CERFA', 2);
define('COTIS', 3);

const PATTERNS_LIST = [
    null                           => 'Aucun, le numéro sera à spécifier manuellement pour chaque document',
    '%{type}-%{year}-%{ynumber}'   => 'Type-Année-Numéro du document par année ("FACT-2021-42")',
    '%{year}-%{type}-%04{ynumber}' => 'Année-Type-Numéro du document par année ("2021-DEVIS-0042")',
    '%{t}-%{year}-%{ynumber}'      => 'Type court-Année-Numéro du document par année ("F-2021-42")',
    '%{y}%{t}%{ynumber}'           => 'Année courte-Type court-Numéro du document par année ("21D42")',
    '%{type}-%{id}'                => 'Type - Numéro unique du document ("FACT-42")',
    '%{t}%{id}'                    => 'Type court et numéro unique du document ("F42")',
    '%{id}'                        => 'Numéro unique du document ("42"))',
    '%06{id}'                      => 'Numéro unique du document sur 6 chiffres ("000042")',
];

$client = new Client;
$facture = new Facture;

$tpl->assign('www_url', \Garradin\WWW_URL);
$tpl->assign('f_obj', $facture);
$tpl->assign('plugin_url', Utils::plugin_url());

$identite = (string) Config::getInstance()->get('champ_identite');

$tpl->register_function('money_fac', function (array $params)
    {
        static $params_list = ['value', 'name', 'user'];

        // Extract params and keep attributes separated
        $attributes = array_diff_key($params, array_flip($params_list));
        $params = array_intersect_key($params, array_flip($params_list));
        extract($params, \EXTR_SKIP);

        $current_value = null;

        if (isset($value)) {
            $current_value = $value;
        }

        if (!isset($user)) {
            $user = false;
        }   

        if (!isset($name))
        {
           $name = 'prix[]'; 
        }

        if (null !== $current_value && !$user) {
            $current_value = Utils::money_format($current_value, ',', '');
        }

        $current_value = htmlspecialchars($current_value, ENT_QUOTES, 'UTF-8');
        
        $currency = Config::getInstance()->get('monnaie');
        return sprintf('<td><nobr><input type="text" pattern="[0-9]*([.,][0-9]{1,2})?" inputmode="decimal" size="8" class="money" style="width: 60%%" onchange="updateSum();" name="%s" value="%s" /><b>%s</b></nobr></td>', $name, $current_value, $currency);
    }
);
