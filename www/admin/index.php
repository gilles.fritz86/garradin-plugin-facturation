<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);


$list = $facture->list();
$list->loadFromQueryString();

$tpl->assign(compact('list'));

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');
