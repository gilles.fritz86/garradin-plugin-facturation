<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

qv(['id' => 'required|numeric']);

$id = (int) qg('id');

$f = $facture->get($id);

if (!$client)
{
	throw new UserException("Ce document n'existe pas.");
}

if (f('delete'))
{
	$form->check('delete_doc_'.$f->id);

	if (!$form->hasErrors())
	{
		try {
			$facture->delete($f->id);
			Utils::redirect(PLUGIN_URL . 'index.php');
	}
	catch (UserException $e)
	{
		$form->addError($e->getMessage());
	}
    }
}

$tpl->assign('doc', $f);
$tpl->display(PLUGIN_ROOT . '/templates/facture_supprimer.tpl');