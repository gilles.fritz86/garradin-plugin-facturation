<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN);

if (f('save') && $form->check('facturation_config'))
{
	try {
		$plugin->setConfig('rna_asso', trim(f('rna_asso')));
		$plugin->setConfig('siret_asso', trim(f('siret_asso')));
		$plugin->setConfig('ttc', (bool)trim(f('ttc')));

		$plugin->setConfig('numero_rue_asso', trim(f('numero_rue_asso')));
		$plugin->setConfig('rue_asso', trim(f('rue_asso')));
		$plugin->setConfig('cp_asso', trim(f('cp_asso')));
		$plugin->setConfig('ville_asso', trim(f('ville_asso')));

		$plugin->setConfig('droit_art200', (bool)f('droit_art200'));
		$plugin->setConfig('droit_art238bis', (bool)f('droit_art238bis'));
		$plugin->setConfig('droit_art885-0VbisA', (bool)f('droit_art885-0VbisA'));
		$plugin->setConfig('objet_0', trim(f('objet_0')));
		$plugin->setConfig('objet_1', trim(f('objet_1')));
		$plugin->setConfig('objet_2', trim(f('objet_2')));;

		$plugin->setConfig('footer', f('footer'));
		
		$plugin->setConfig('validate_cp', (bool)f('validate_cp'));
		$plugin->setConfig('unique_client_name', (bool)f('unique_client_name'));

        $plugin->setConfig('pattern', f('pattern'));

		Utils::redirect(PLUGIN_URL . 'config.php?ok');
    }
    catch (UserException $e)
    {
	    $form->addError($e->getMessage());
    }
}


$tpl->assign('ok', qg('ok') !== null);

$tpl->assign('patterns', \Garradin\Plugin\Facturation\PATTERNS_LIST);

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');
