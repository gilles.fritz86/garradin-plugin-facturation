<?php

namespace Garradin;
use Garradin\Plugin\Facturation\Facture;
use Garradin\Entities\Files\File;

define('DEVIS', 0);
define('FACT', 1);
define('CERFA', 2);
define('COTIS', 3);

$db = DB::getInstance();
$facture = new Facture;
$infos = $plugin->getInfos();


// 0.2.0 - Stock le contenu en json plutôt qu'en serialized
if (version_compare($infos->version, '0.2.0', '<'))
{
	$r = (array) DB::getInstance()->get('SELECT * FROM plugin_facturation_factures');
	
	foreach ($r as $e) {
		$e->contenu =json_encode(unserialize((string) $e->contenu));
		$db->update('plugin_facturation_factures', $e, $db->where('id', (int)$e->id));
	}
}

// 0.3.0 - Migration Facturation\Config vers la table plugins
if (version_compare($infos->version, '0.3.0', '<'))
{
	$conf = $db->getAssoc('SELECT cle, valeur FROM plugin_facturation_config ORDER BY cle;');
	foreach($conf as $k=>$v)
	{
		if(!$plugin->setConfig($k, $v))
		{
			throw new UserException('Erreur dans la conversion de la configuration pour la clé : '.$k);
		}
	}
	$db->exec('DROP TABLE `plugin_facturation_config`;');
}

// 0.4.0 - 
if (version_compare($infos->version, '0.4.0', '<'))
{
	$db->exec(<<<EOT
		CREATE TABLE IF NOT EXISTS plugin_facturation_paiement
		-- Moyens de paiement
		(
			code TEXT NOT NULL PRIMARY KEY,
			nom TEXT NOT NULL
		);

		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('CB', 'Carte bleue');
		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('CH', 'Chèque');
		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('ES', 'Espèces');
		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('PR', 'Prélèvement');
		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('TI', 'TIP');
		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('VI', 'Virement');
		INSERT OR IGNORE INTO plugin_facturation_paiement (code, nom) VALUES ('AU', 'Autre');

			
		CREATE TABLE IF NOT EXISTS plugin_facturation_factures_tmp
		(
			id					INTEGER PRIMARY KEY,
			type_facture		INTEGER NOT NULL DEFAULT 0,
			numero				TEXT NOT NULL UNIQUE,
			receveur_membre 	INTEGER NOT NULL, -- bool
			receveur_id			INTEGER NOT NULL,
			date_emission		TEXT NOT NULL, -- CHECK (date(date_emission) IS NOT NULL AND date(date_emission) = date_emission),
			date_echeance		TEXT NOT NULL, -- CHECK (date(date_echeance) IS NOT NULL AND date(date_echeance) = date_echeance),
			reglee				INTEGER DEFAULT 0, -- bool
			archivee			INTEGER DEFAULT 0, -- bool
			moyen_paiement		TEXT NOT NULL,
			contenu 			TEXT NOT NULL,
			total   			REAL DEFAULT 0
		);

		INSERT INTO plugin_facturation_factures_tmp SELECT * FROM plugin_facturation_factures;
		DROP TABLE plugin_facturation_factures;
		ALTER TABLE plugin_facturation_factures_tmp RENAME TO plugin_facturation_factures;

EOT
);

}

// 0.6.0 - 
if (version_compare($infos->version, '0.6.0', '<'))
{
	$r = $db->first('SELECT id, total FROM plugin_facturation_factures;');
	if (strpos($r->total,'.'))
	{
		// SQL -> total integer
		$db->exec(<<<EOT
		CREATE TABLE IF NOT EXISTS plugin_facturation_factures_tmp
		(
			id					INTEGER PRIMARY KEY,
			type_facture		INTEGER NOT NULL DEFAULT 0,
			numero				TEXT NOT NULL UNIQUE,
			receveur_membre 	INTEGER NOT NULL, -- bool
			receveur_id			INTEGER NOT NULL,
			date_emission		TEXT NOT NULL, -- CHECK (date(date_emission) IS NOT NULL AND date(date_emission) = date_emission),
			date_echeance		TEXT NOT NULL, -- CHECK (date(date_echeance) IS NOT NULL AND date(date_echeance) = date_echeance),
			reglee				INTEGER DEFAULT 0, -- bool
			archivee			INTEGER DEFAULT 0, -- bool
			moyen_paiement		TEXT NOT NULL,
			contenu 			TEXT NOT NULL,
			total   			INTEGER DEFAULT 0
		);
		
		INSERT INTO plugin_facturation_factures_tmp SELECT * FROM plugin_facturation_factures;
		DROP TABLE plugin_facturation_factures;
		ALTER TABLE plugin_facturation_factures_tmp RENAME TO plugin_facturation_factures;
EOT
	);

		$factures = $facture->listAll();
		foreach($factures as $k=>$f)
		{
			foreach($f->contenu as $line => $content)
			{
				// Petit bug qui peut arriver avec des contenus mal enregistrés en db
			   if (is_int($content)) 
			   {
				   continue;
			   }

				$contenu[] = ['designation' => $content['designation'],
					'prix' => (int) ($content['prix'] * 100) ];
			}

			$f->contenu = $contenu;
			$data = (array) $f;
			$data['total'] = (int) ($data['total'] * 100);
			unset($data['id']);
			unset($data['date_emission']);
			unset($data['date_echeance']);
			$facture->edit($f->id, $data);
			unset($contenu);
		}

		$path = __DIR__.'/data/default_sign.png';
		$png = (new File)->createAndStore('skel/plugin/facturation','sign.png', $path, null);
	}
}

// 0.6.2 - 
if (version_compare($infos->version, '0.6.2', '<'))
{
	$db->exec(<<<EOT
	    INSERT OR IGNORE INTO plugin_facturation_paiement
	    (code, nom) VALUES ('HA', 'HelloAsso');
	    INSERT OR IGNORE INTO plugin_facturation_paiement
	    (code, nom) VALUES ('AU', 'Autre');

	    CREATE TABLE IF NOT EXISTS
	    plugin_facturation_txt_cerfa
        (
            id PRIMARY KEY,
            menu TEXT NOT NULL UNIQUE,
            texte TEXT NOT NULL
        );

        INSERT OR IGNORE INTO plugin_facturation_txt_cerfa
        ("id","menu","texte") VALUES ('0','Aucun','');
        INSERT OR IGNORE INTO plugin_facturation_txt_cerfa
        ("id","menu","texte") 
        VALUES ('1','HelloAsso','Don via HelloAsso');
        INSERT OR IGNORE INTO plugin_facturation_txt_cerfa
        ("id","menu","texte") 
        VALUES ('2','Frais de déplacement',
        'Renonciation aux remboursements de frais de déplacement');
        INSERT OR IGNORE INTO plugin_facturation_txt_cerfa
        ("id","menu","texte") 
        VALUES ('3','Don en nature','Don en nature');
EOT
	);

	// Migration CERFA
	$factures = $facture->listAll();
	foreach($factures as $k=>$f)
	{
		if ($f->type_facture != CERFA)
		{
			continue;
		}

		$f->contenu = ['forme' => 1, 'nature' => 1, 'texte' => 0];
		$data = (array) $f;
		unset($data['id']);
		unset($data['date_emission']);
		unset($data['date_echeance']);
		$facture->edit($f->id, $data);
	}
}

// 0.7.1 - Ajout clé config TTC/HT
if (version_compare($infos->version, '0.7.1', '<'))
{
	$plugin->setConfig('ttc', false);
}